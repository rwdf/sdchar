# Shadowdark Character Generator
## TODO:

[x] Roll stats \
[x] Calculate modifiers \
[] Pick class based on best stat or offer best choice \
[] Optional re-roll when no stat over 14 OR make it a Roustabout \
[] Roll for ancestry \
[] Roll for name \
[] Roll for background \
[] Roll for alignment \
[] Roll for talents \
[] Pick spells for classes that are magic users \
[] Roll for title \
[] Take in command line arguments for ancestry and class \
[] look at the rules to find out more ... 

