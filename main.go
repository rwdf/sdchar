package main

import (
	"fmt"
	"math/rand/v2"
)

type Character struct {
	Gear       map[string]int
	Class      Class
	Ancestry   string
	Title      string
	Languages  []string
	Deities    []string
	Stats      Stats
	Background Background
}

type Stats struct {
	Strength     Stat
	Dexterity    Stat
	Constitution Stat
	Intelligence Stat
	Wisdom       Stat
	Charisma     Stat
}

type Stat struct {
	value    int
	modifier int
}

type Class struct {
	Name         string
	Weapons      []string
	Armor        []string
	HitPoints    string
	Specialities []string
	Spellcasting bool
}

type Background struct {
	Title   string
	Details string
}

var (
	urchin        = Background{"Urchin", "You grew up in the merciless streets of a large city."}
	wanted        = Background{"Wanted", "There's a price on your head, but you have allies."}
	cultInitiate  = Background{"Cult Initiate", "You know blasphemous secrets and rituals."}
	thievesGuild  = Background{"Thieves' Guild", "You have connections, contacts and debts."}
	banished      = Background{"Banished", "Your people cast you out for supposed crimes."}
	orphaned      = Background{"Orphaned", "An unusual guardian rescued and raised you."}
	wizApprentice = Background{"Wizard's Apprentice", "You have a knack and eye for magic"}
	jeweler       = Background{"Jeweler", "You can easily appraise value and authenticity."}
	herbalist     = Background{"Herbalist", "You know plants, medicines, and poisons."}
	barbarian     = Background{"Barbarian", "You left the horde, but it never quite left you."}
	merenary      = Background{"Mercenary", "You fought friend and foe alike for your coin."}
	sailor        = Background{"Sailor", "Pirate, privateer, or merchant — the seas are yours."}
	acolyte       = Background{"Acolyte", "You're well trained in religious rites and doctrines."}
	solider       = Background{"Soldier", "You served as a fighter in an organized army."}
	ranger        = Background{"Ranger", "The woods and wilds are your true home."}
	scout         = Background{"Scout", "You survived on stealth, observation, and speed."}
	minstrel      = Background{"Minstrel", "You've traveled far with your charm and talent."}
	scholar       = Background{"Scholar", "You know much about ancient history and lore."}
	noble         = Background{"Noble", "A famous name has opened many doors for you."}
	chirurgeon    = Background{"Chirurgeon", "You know anatomy, surgery, and first aid."}
)

func main() {
	stats := rollStats()
	fmt.Printf("Rolled stats %+v \n", stats)
	if stats.checkMinValues() {
		fmt.Println("All stats under 14, reroll or make a Roustabout?")
	}
}

func (stats Stats) checkMinValues() bool {
	minval := 14
	return stats.Strength.value < minval &&
		stats.Dexterity.value < minval &&
		stats.Constitution.value < minval &&
		stats.Intelligence.value < minval &&
		stats.Wisdom.value < minval &&
		stats.Charisma.value < minval
}

func (stats Stats) findBestStat() string {
	STR := stats.Strength.value
	DEX := stats.Dexterity.value
	CON := stats.Constitution.value
	INT := stats.Intelligence.value
	WIS := stats.Wisdom.value
	CHA := stats.Charisma.value

	highest := max(STR, DEX, CON, INT, WIS, CHA)

	switch highest {
	case STR:
		return "Strength"
	case DEX:
		return "Dexterity"
	case CON:
		return "Constitution"
	case INT:
		return "Intelligence"
	case WIS:
		return "Wisdom"
	case CHA:
		return "Charisma"
	default:
		return "Inconclusive"
	}
}

func rollStats() Stats {
	var stats Stats
	stats.Strength.value = roll(3, 6)
	stats.Strength.modifier = findStatModifier(stats.Strength.value)
	stats.Dexterity.value = roll(3, 6)
	stats.Dexterity.modifier = findStatModifier(stats.Dexterity.value)
	stats.Constitution.value = roll(3, 6)
	stats.Constitution.modifier = findStatModifier(stats.Constitution.value)
	stats.Intelligence.value = roll(3, 6)
	stats.Intelligence.modifier = findStatModifier(stats.Intelligence.value)
	stats.Wisdom.value = roll(3, 6)
	stats.Wisdom.modifier = findStatModifier(stats.Wisdom.value)
	stats.Charisma.value = roll(3, 6)
	stats.Charisma.modifier = findStatModifier(stats.Charisma.value)
	return stats
}

func findStatModifier(value int) (mod int) {
	return (value - 10) / 2
}

func roll(num, die int) (result int) {
	if num <= 1 {
		return rand.IntN(die) + 1
	}
	return rand.IntN(die) + 1 + roll(num-1, die)
}
