package main

import (
	"testing"
)

func TestCheckMinValues_tooLow(t *testing.T) {
	tooLowStats := Stats{
		Strength:     Stat{value: 13},
		Dexterity:    Stat{value: 13},
		Constitution: Stat{value: 13},
		Intelligence: Stat{value: 13},
		Wisdom:       Stat{value: 13},
		Charisma:     Stat{value: 13},
	}
	want := true
	got := tooLowStats.checkMinValues()
	if want != got {
		t.Fatalf("wanted %v got %v", want, got)
	}
}

func TestCheckMinValues(t *testing.T) {
	tooLowStats := Stats{
		Strength:     Stat{value: 14},
		Dexterity:    Stat{value: 13},
		Constitution: Stat{value: 13},
		Intelligence: Stat{value: 13},
		Wisdom:       Stat{value: 13},
		Charisma:     Stat{value: 13},
	}
	want := false
	got := tooLowStats.checkMinValues()
	if want != got {
		t.Fatalf("wanted %v got %v", want, got)
	}
}

func TestFindBestStat_Strength(t *testing.T) {
	stats := Stats{
		Strength:     Stat{value: 14},
		Dexterity:    Stat{value: 13},
		Constitution: Stat{value: 13},
		Intelligence: Stat{value: 13},
		Wisdom:       Stat{value: 13},
		Charisma:     Stat{value: 13},
	}

	want := "Strength"
	got := stats.findBestStat()

	if want != got {
		t.Fatalf("wanted %v, got %v", want, got)
	}
}

func TestFindBestStat_TwoOfSameValue(t *testing.T) {
	stats := Stats{
		Strength:     Stat{value: 13},
		Dexterity:    Stat{value: 14},
		Constitution: Stat{value: 14},
		Intelligence: Stat{value: 13},
		Wisdom:       Stat{value: 13},
		Charisma:     Stat{value: 13},
	}

	want := "Inconclusive"
	got := stats.findBestStat()

	if want != got {
		t.Fatalf("wanted %v, got %v", want, got)
	}
}
